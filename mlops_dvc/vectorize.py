import pickle
from pathlib import Path

import click
import unicodedata
import dvc.api
import numpy as np
import polars as pl
import scipy as sp
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

from .cli import cli

def normalize_unicode(text):
    return unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').decode()


def train_vectorize(
        data: pl.DataFrame,
) -> tuple[TfidfVectorizer, pl.DataFrame, pl.DataFrame]:
    vectorizer_params = dvc.api.params_show()

    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params['vectorizer_tfidf']) 

    train, test = train_test_split(
        data,
        test_size=vectorizer_params['test_train_split'],
        shuffle=False,
        random_state=vectorizer_params['random_state']
    )
    tfidf_vectorizer.fit(train["corpus"].list.join(" ").to_numpy())
    return tfidf_vectorizer, train, test

def apply_vectorizer(vectorizer: TfidfVectorizer, data: pl.DataFrame) -> pl.DataFrame:
    return vectorizer.transform(data["corpus"].list.join(" ").to_numpy())

@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("vectorizer_path", type=Path)
@click.argument("train_features_path", type=Path)
@click.argument("test_features_path", type=Path)
def cli_train_vectorizer(
    input_frame_path: Path, 
    vectorizer_path: Path, 
    train_features_path: Path, 
    test_features_path: Path,
):
    data = pl.read_parquet(input_frame_path)
    vectorizer, train, test = train_vectorize(data)
    with open(vectorizer_path, 'wb') as file: 
        pickle.dump(vectorizer, file)
    
    train.write_parquet(train_features_path)
    test.write_parquet(test_features_path)

@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("vectorizer_path", type=Path)
@click.argument("result_frame_path", type=Path)
@click.argument("target_frame_path", type=Path)
def cli_apply_vectorizer(
    input_frame_path: Path,
    vectorizer_path: Path,
    result_frame_path: Path,
    target_frame_path: Path,
):
    data = pl.read_parquet(input_frame_path)
    vectorizer = pickle.load(vectorizer_path.open("rb"))
    result = apply_vectorizer(vectorizer, data)
    sp.sparse.save_npz(result_frame_path, result)
    np.save(target_frame_path, data["Polarity"].to_numpy())